class Auth0Controller < ActionController::Base
  def callback
  if params[:user]
    signup if params[:signup]
    session[:token_id] = login
  else
    session[:token_id] = google_login
  end
  redirect_to '/dashboard'
  rescue Auth0::Unauthorized
    redirect_to '/', notice: 'Invalid email or password'
  rescue => ex
    redirect_to '/', notice: ex.message
end
end